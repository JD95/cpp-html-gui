#include "IdList.h"

IdList::IdList()
	: inactive_ids(0)
{
	for (uint32_t i = 0; i < 1000; i++)
	{
		ids.emplace_back(0, i);
	}
}

Id IdList::get_id()
{
	auto id = ids.at(inactive_ids++);
	if (inactive_ids >= ids.size()) {
		for (uint32_t i = 0; i < 1000; i++)
		{
			ids.emplace_back(0, id.tag() + i);
		}
	}
	return id;
}

void IdList::free_id(const Id& to_free)
{
	auto pos = std::find(ids.begin(), ids.begin() + inactive_ids, to_free);
	*pos = Id(pos->version() + 1, pos->tag());
	inactive_ids--;
	std::iter_swap(pos, ids.begin() + inactive_ids);
}

Id::Id()
	: id_version(0), id_tag(0)
{
}

Id::Id(uint32_t v, uint32_t t)
	: id_version(v), id_tag(t)
{}

uint32_t Id::tag() const
{
	return id_tag;
}

uint32_t Id::version() const
{
	return id_version;
}

std::string Id::to_string() const
{
	return std::to_string(id_version) + std::to_string(id_tag);
}