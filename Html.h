#pragma once

#include <string>
#include <vector>
#include <functional>
#include "IdList.h"

using std::string;
using std::vector;
using std::function;

using Name = std::string;
using Text = std::string;

enum HtmlTag {
	H1,
	H2,
	H3,
	H4,
	H5,
	IMG,
	BUTTON,
	UL,
	TR,
	TD,
	TABLE,
	TEXT,
	LI,
	P
};

struct Html
{
	Id id;
	HtmlTag tag;
	Text text;
	Name name;
	vector<Html> contents;

	Html(Text _text);
	Html(HtmlTag _tag, Text _text);
	Html(HtmlTag _tag, vector<Html> _contents);
	~Html();

	Html* find_named_item(Name n);
};

Html txt(Text text);
Html h1(Text text);
Html h2(Text text);
Html h3(Text text);
Html h4(Text text);
Html h5(Text text);
Html img(Text url);
Html ul(vector<Html> elems);
Html row(vector<Html> elems);
Html table(vector<Html> rows);
Html button(Text text);
Html named(Name name, Html elem);
Html p(Text text);