#include "webpage.h"
#include <numeric>
#include <algorithm>


WebPage::WebPage(std::string ip, int port)
	: server(ip, port)
{
	std::cout << "Waiting for webpage to connect...\n";
	client = server.accept_connection();
	std::cout << "Webpage connected!\n";
}

void WebPage::add(Html& item)
{
	item.id = elem_ids.get_id();

	if (item.name != "") {
		name_table.insert(std::make_pair(item.id.to_string(), item.name));
	}

	string message = "add," + std::to_string(item.tag) + "," + item.id.to_string();
	if (item.text != "") {
		message += "," + item.text;
	}
	client.send_message(message);

	for (auto& elem : item.contents) {
		add_item_to(elem, item);
	}
}

void WebPage::add_item_to(Html& item, const Html& parent)
{
	item.id = elem_ids.get_id();

	if (item.name != "") {
		name_table.insert(std::make_pair(item.id.to_string(), item.name));
	}

	string message = "addto," + std::to_string(item.tag) + "," + parent.id.to_string() + "," + item.id.to_string();
	if (item.text != "") {
		message += "," + item.text;
	}
	client.send_message(message);

	for (auto& elem : item.contents) {
		add_item_to(elem, item);
	}
}

void WebPage::remove(Html item)
{
	string message = "remove," + item.id.to_string();
	client.send_message(message);
}

void WebPage::lock(Html item)
{
	string message = "lock," + item.id.to_string();
	client.send_message(message);
}

void WebPage::react(std::function<bool(Name, Event)> f)
{
	bool keep_going = true;
	do {
		ClientMessage e = client.recv_message(256);
		string id = e.message.substr(0, e.message.find(","));
		string evnt = e.message.substr(e.message.find(",") + 1, e.message.length());
		if (name_table.find(id) != name_table.end()) {
			if (evnt == "0") {
				keep_going = f(name_table.at(id), CLICKED);
			}
		}
	} while (keep_going);
}

void WebPage::change_text(Html & item, Text new_text)
{
	item.text = new_text;
	string message = "changetext," + item.id.to_string() + "," + new_text;
	client.send_message(message);
}
