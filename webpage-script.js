function lookup_tag(tag) {
    switch(Number(tag)) {
    case 0: return "h1";
    case 1: return "h2";
    case 2: return "h3";
    case 3: return "h4";
    case 4: return "h5";
    case 5: return "img";
    case 6: return "button";
    case 7: return "ul";
    case 8: return "tr";
    case 9: return "TD";
    case 10: return "table";
    case 11: return "text"
    case 12: return "li";
    case 13: return "p";
    }
}

function construct_element(tag, id, text) {
    if(tag === "img"){
        return "<img src=\"" + text + "\" id=\"" + id + "\" />";
    }
    else if (tag === "button"){
        return "<button type=\"button\" id=\"" + id + "\">" + text + "</button>";
    }
    else {
        return "<" + tag + " id=\"" + id + "\">" + text + "</" + tag + ">";
    }
}

function add(tag, id, text) {
    var elem = "<div class=\"row\">" + construct_element(tag,id,text) + "</div>";
    $(".container").append(elem);
}

function addto(parent, child_tag, child_id, child_text) {
    var child = construct_element(child_tag, child_id, child_text);
    $("#" + parent).append(child);
}

function remove(id) {
    $("#" + id).remove();
}

function lock(id) {
    console.log("Locking " + id);
}

function add_event_listeners(server_socket, id) {
    $("#" + id).click(function() {
        server_socket.send(id + "," + "0");
    });

}

function change_item_text(id, text){
    $("#" + id).text(text);
}

var server = new WebSocket("ws://127.0.0.1:4040/");

server.onmessage = function(event) {
    var items = event.data.split(',');
    var cmd = items[0];
    var content = items.splice(1);
    if (cmd === "add"){
        var item_tag = lookup_tag(content[0]);
        var item_id = content[1];
        var item_text = content.length > 2 ? content[2] : "";
        add(item_tag, item_id, item_text);
        add_event_listeners(server, item_id);
    }
    else if (cmd === "addto") {
        var item_tag = lookup_tag(content[0]);
        var parent = content[1];
        var item_id = content[2];
        var item_text = content.length > 3 ? content[3] : "";
        addto(parent, item_tag, item_id, item_text);
        add_event_listeners(server, item_id);
    }
    else if (cmd === "remove") {
        remove(content[0]);
    }
    else if (cmd === "lock") {
        lock(content[0]);
    }
    else if (cmd === "changetext"){
        var item_id = content[0];
        var new_text = content[1];
        change_item_text(item_id, new_text);
    }
};
