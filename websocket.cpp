#include "websocket.h"
#include "sha1.h"
#include "base64.h"

const char * ClientStartupException::what() const throw () {
	return "client failed to startup";
}

const char * ClientWindowsSocketStartupException::what() const throw () {
	return "the windows socket library failed to initalize";
}

const char * ClientSocketCreationException::what() const throw () {
	return "the client could not create a socket";
}

const char * ClientConnectionException::what() const throw () {
	return "the client could not start a connection with the server";
}

const char * ClientSendException::what() const throw () {
	return "the client could not send the message to the server";
}

const char * ClientRecvException::what() const throw () {
	return "the client could not recieve the message from the server";
}


bool Server::wsa_initialized = false;

Server::Server(Ip ip, Port port) {
	// initialize winsock if it has not already been initalized
	if (!wsa_initialized) {
		int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != 0) {
			printf("WSAStartup failed with error: %d\n", iResult);
			throw ClientWindowsSocketStartupException();
		}
		wsa_initialized = true;
	}

	// create socket
	ConnectSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (ConnectSocket == INVALID_SOCKET) {
		wprintf(L"socket function failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		throw ClientSocketCreationException();
	}

	// create address
	IN_ADDR test;
	InetPton(AF_INET, ip.c_str(), &test);

	sockaddr_in clientService;
	clientService.sin_family = AF_INET;
	clientService.sin_addr = test;
	clientService.sin_port = htons(port);

	// connect to server
	int iResult = bind(ConnectSocket, (SOCKADDR *) & clientService, sizeof(clientService));
	if (iResult == SOCKET_ERROR) {
		wprintf(L"connect function failed with error: %ld\n", WSAGetLastError());
		iResult = closesocket(ConnectSocket);
		if (iResult == SOCKET_ERROR)
			wprintf(L"closesocket function failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		throw ClientConnectionException();
	}
}

 Server::~Server() {
	closesocket(ConnectSocket);
	WSACleanup();
}

ClientMessage Server::server_recv(int recv_buffer_length, SOCKET client) {
	std::vector<char> recv_buff(recv_buffer_length);
	int iResult = recv(client, &recv_buff[0], recv_buffer_length, 0);
	if (iResult > 0) {
		auto s = std::string(&recv_buff[0]);
		//s = s.substr(0, s.find('\r\n\r\n'));
		return ClientMessage{s , iResult };
	}
	else {
		printf("recv failed with error: %d\n", WSAGetLastError());
		throw ClientRecvException();
	}
}

BytesSent Server::server_send(Message s, SOCKET client) {
	auto iResult = send(client, s.c_str(), (int)s.length(), 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		throw ClientSendException();
	}
	return iResult;
}

Client Server::accept_connection()
{
	SOCKET client_socket;
	int iResult = listen(ConnectSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return Client();
	}

	// Accept a client socket
	client_socket = accept(ConnectSocket, NULL, NULL);
	if (client_socket == INVALID_SOCKET) {
		printf("accept failed with error: %d\n", WSAGetLastError());
		closesocket(client_socket);
		WSACleanup();
		return Client();
	}

	ClientMessage header = server_recv(1028, client_socket);

	int key_pos = header.message.find("Sec-WebSocket-Key: ");
	std::string key = header.message.substr(key_pos + 19, 24);

	std::string magic_string = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

	SHA1 checksum;
	checksum.update(key + magic_string);
	std::string sec_key_base10 = checksum.final();
	std::string sec_key_base64;
	Base64::Encode(sec_key_base10, &sec_key_base64);

	std::string hand_shake = "HTTP/1.1 101 Switching Protocols\r\n";
	hand_shake += "Sec-WebSocket-Accept: " + sec_key_base64 + "\r\n";
	hand_shake += "Connection: Upgrade\r\n";
	hand_shake += "Upgrade: websocket\r\n";
	hand_shake += "\r\n";

	server_send(hand_shake, client_socket);

	return Client{ client_socket };
}

void Client::send_message(Message message)
{
	auto chunk_begin = message.begin();
	for (int i = 0; i < message.size(); i += 125) {
		auto chunk_length = i + 125 > message.size() ? message.size() - i : 125;
		auto chunk_end = message.begin() + i + chunk_length;
		std::string chunk = std::string(message.begin() + i, chunk_end);

		uint8_t finn = 0;
		if (i + 125 >= message.size()) {
			// Mark as "final" packet
			finn ^= 1UL << 7;
		}
		if (i == 0) {
			// Mark as text payload
			finn ^= 1UL << 0;
		}

		uint8_t mask = chunk.size();
		// Disable Masking
		mask &= ~(1UL << 7);

		std::string result = "";

		result += std::string(1, finn);
		result += std::string(1, mask);
		result += chunk;

		auto iResult = send(client, result.c_str(), (int)result.length(), 0);
		if (iResult == SOCKET_ERROR) {
			printf("send failed with error: %d\n", WSAGetLastError());
			throw ClientSendException();
		}
	}
}

ClientMessage Client::recv_message(int recv_buffer_length)
{
	std::string message;
	std::vector<char> recv_buff(recv_buffer_length);
	int iResult = recv(client, &recv_buff[0], recv_buffer_length, 0);
	if (iResult > 0) {
		message = std::string(&recv_buff[0]);
	}
	else {
		printf("recv failed with error: %d\n", WSAGetLastError());
		throw ClientRecvException();
	}
	uint8_t finn = message[0];
	if (finn != 129) return { "ERROR UNFRAMING MESSAGE! NOT TEXT PAYLOAD!", 0 };
	uint8_t length = message[1];
	if ((length >> 7) & 1U) {
		// If the message is masked

		// Flip the masked bit to get the length
		length ^= 1UL << 7;
		if (message.length() - 6 != length) return { "", 0 };
		uint8_t mask[4] = { message[2], message[3], message[4], message[5] };
		std::string result = "";
		for (int i = 0; i < length; i++)
		{
			result += std::string(1, message[6 + i] ^ mask[i % 4]);
		}
		return { result, iResult };
	}

	return ClientMessage();
}
