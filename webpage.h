#pragma once
#include <vector>
#include <iostream>
#include <string>
#include <map>
#include <functional>

#include "websocket.h"
#include "IdList.h"
#include "Html.h"

using std::string;
using std::map;

enum Command {
	ADD,
	REMOVE,
	LOCK,
};

enum Event {
	CLICKED,
};

class WebPage {
	Server server;
	Client client;
	IdList elem_ids;
	map<string, Name> name_table;

public:
	WebPage(string ip, int port);
	void add(Html& item);
	void add_item_to(Html& item, const Html& parent);
	void remove(Html item);
	void lock(Html item);
	void react(std::function<bool(Name, Event)> f);
	void change_text(Html& item, Text new_text);
};