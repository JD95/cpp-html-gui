#include "Html.h"


Html::Html(Text _text)
{
	tag = TEXT;
	text = _text;
	name = "";
}

Html::Html(HtmlTag _tag, Text _text)
{
	tag = _tag;
	text = _text;
	name = "";
}

Html::Html(HtmlTag _tag, vector<Html> _contents)
{
	tag = _tag;
	contents = _contents;
	name = "";
}


Html::~Html()
{
}

Html * Html::find_named_item(Name n)
{
	for (auto& item : contents) {
		if (item.name == n) {
			return &item;
		}
		else {
			Html* result = item.find_named_item(n);
			if (result != NULL) {
				return result;
			}
		}
	}

	return NULL;
}

Html txt(Text text)
{
	return Html(text);
}

Html h1(Text text)
{
	return Html(H1, text);
}

Html h2(Text text)
{
	return Html(H2, text);
}

Html h3(Text text)
{
	return Html(H3, text);
}

Html h4(Text text)
{
	return Html(H4, text);
}

Html h5(Text text)
{
	return Html(H5, text);
}

Html img(Text url)
{
	return Html(IMG, url);
}

Html ul(vector<Html> elems)
{
	vector<Html> list_items;
	for (auto elem : elems) {
		list_items.push_back(Html(LI, { elem }));
	}
	return Html(UL, list_items);
}

Html row(vector<Html> elems)
{
	vector<Html> row_items;
	for (auto elem : elems) {
		row_items.push_back(Html(TD, { elem }));
	}
	return Html(TR, elems);
}

Html table(vector<Html> rows)
{
	vector<Html> items;
	for (auto row : rows) {
		items.push_back(Html(TR, { row }));
	}

	return Html(TABLE, rows);
}

Html button(Text text)
{
	return Html(BUTTON, text);
}

Html named(Name name, Html elem)
{
	elem.name = name;
	return elem;
}

Html p(Text text)
{
	return Html(P, text);
}
