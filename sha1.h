#ifndef SHA1_HPP
#define SHA1_HPP


#include <cstdint>
#include <iostream>
#include <string>


union CharBytes
{
	uint32_t number;
	char bytevalue[4];

};


class SHA1
{
public:
	SHA1();
	void update(const std::string &s);
	void update(std::istream &is);
	std::string final();
	static std::string from_file(const std::string &filename);

private:
	uint32_t digest[5];
	std::string buffer;
	uint64_t transforms;
};
#endif 