#pragma once
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <exception>
#include <vector>
#include <string>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

struct ClientMessage {
	std::string message;
	int bytes_recieved;
};

using BytesSent = int;
using Message = std::string;
using Port = int;
using Ip = std::string;

struct Client {
	SOCKET client;

	void send_message(Message s);
	ClientMessage recv_message(int recv_buffer_length);
};

class Server {
	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;

public:
	static bool wsa_initialized;

	Server(Ip ip, Port port);

	~Server();

	ClientMessage server_recv(int recv_buffer_length, SOCKET client);

	BytesSent server_send(Message s, SOCKET client);

	Client accept_connection();
};

// Client Exceptions
struct ClientStartupException : public std::exception {
	const char * what() const throw ();
};

struct ClientWindowsSocketStartupException : public std::exception {
	const char * what() const throw ();
};

struct ClientSocketCreationException : public std::exception {
	const char * what() const throw ();
};

struct ClientConnectionException : public std::exception {
	const char * what() const throw ();
};

struct ClientSendException : public std::exception {
	const char * what() const throw ();
};

struct ClientRecvException : public std::exception {
	const char * what() const throw ();
};

