#pragma once
#include <vector>
#include <iostream>
#include <string>

class Id {
	uint32_t id_version;
	uint32_t id_tag;
public:
	Id();
	Id(uint32_t v, uint32_t t);
	uint32_t tag() const;
	uint32_t version() const;
	friend bool operator== (const Id& a, const Id& b) {
		return a.id_tag == b.id_tag && a.id_version == b.id_version;
	}
	std::string to_string() const;
};

class IdList {
	size_t inactive_ids;
	std::vector<Id> ids;
public:

	IdList();
	Id get_id();
	void free_id(const Id& to_free);
};