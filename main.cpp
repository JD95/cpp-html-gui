#include <string>
#include <vector>
#include <iostream>
#include "websocket.h"
#include "webpage.h"
#include "Html.h"

using namespace std;

bool is_button(Name n) {
	return n.substr(0, 6) == "button";
}

string turn_symbol(int turn) {
	return turn % 2 == 0 ? "X" : "O";
}

int main() {

	// To begin the program we'll create something to listen
	// for events on the webpage. When first launched, the program
	// will wait for the index.html page to be opened or refreshed
	// so it can start interacting.
	WebPage page("127.0.0.1", 4040);

	// This section adds some elements to the webpage
	// Every item will be added into a new row on the page
	// So you wont get items stack up horizontally
	page.add(h1("Tic Tac Toe!"));
	page.add(p("Welcome to the game of tic tac toe!"));
	page.add(h3("Rules:"));
	page.add(ul({ 
		txt("You cannot move on a spot already taken"), 
		txt("Three in a row wins") 
		}));

	// Here we create an html item that we're going to use later
	// To add to the page we just pass in the variable here
	auto game_message = h2("It is Player X's turn");
	page.add(game_message);

	// Here we create a table filled with "named" items
	// If we give a name to an item, we can listen for 
	// events that happen to it later, like when the item
	// is clicked!
	//
	// To name an html element, use the `named()` function
	// on it, and you will get a named version of the item.
	vector<Html> rows;
	for (int i = 0; i < 3; i++)
	{
		vector<Html> items;
		for (int j = 0; j < 3; j++)
		{
			string spot_number = to_string((i * 3) + j);
			Name spot = "button" + spot_number;
			items.push_back(named(spot, button(spot_number)));
		}
		rows.push_back(row(items));
	}
	auto tbl = table(rows);
	page.add(tbl);

	page.add(named("quit", button("quit the game")));

	// Here is where the logic of the game will be implemented
	// Rather than waiting for input from the console, the program
	// will listen for events from the webpage. If the event happens
	// for an item we named previously then it will call the
	// `handle_events` function we define below. Here it is actually
	// necessary to create an on-the-spot function because we need this
	// function to have access to the webpage and other variables defined
	// here in main.
	string moves = "";
	int turn = 0;
	auto handle_events = [&](Name n, Event e) -> bool {
		// When an event goes off, we will get two pieces of information
		// The name of the item involved in the event, and the type of 
		// event that occured.
		if (is_button(n)) {
			// Now that we know a button was involved in the event we 
			// need to get access to that button inside of `tbl`. To do
			// this we can use the `find_named_item` function from the
			// Html class.
			//
			// This function will look through the contents of the item and
			// try to find something with the name we're looking for. If it
			// succeeds it will return a pointer to that object. If it fails,
			// then the pointer returned will be NULL or not pointing to anything
			moves += n.substr(6);
			Html* button = tbl.find_named_item(n);
			
			if (button != NULL) {
				// Once we know that the pointer is not NULL we can safely follow
				// that pointer to the button on the other side. We'll want to 
				// pass the button to the `change_text()` function from the webpage.
				//
				// This function will let us change the text of any html item we've
				// previously added to the webpage.
				page.change_text(*button, turn_symbol(turn));
				turn++;
				page.change_text(game_message,
					"It is Player " + turn_symbol(turn) + "'s turn!");
			}

		}
		else if (n == "quit") {
			// The function `handle_events` will continue to be called until
			// it returns false. So we can use this to quit the program
			return false;
		}

		// If we aren't done listening for events, then we can return true and
		// this function will be called again when the next event arrives
		return true;
	};

	page.react(handle_events);

	cout << "The game is over! These were the moves made: " + moves + "\n";

	system("pause");
	return 0;
}
